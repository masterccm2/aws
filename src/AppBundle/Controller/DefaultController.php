<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }
    
    /**
     * @route("/register", name="register")
     */
    public function registerAction(Request $request)
    {

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('name', TextType::class)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('age', IntegerType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //@TODO Save les data
            }
        }

        return $this->render('default/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //@TODO verifier en base si les infos sont corrects
            }
        }
        return $this->render('default/register.html.twig', array(
            'form' => $form->createView(),
            'ip' => $this->container->get('request_stack')->getCurrentRequest()->getClientIp(),
        ));
    }

    /**
     * @route("/fib", name="fib")
     */
    public function fibAction(Request $request)
    {
        $this->onRequestStart();
        $md5Tab = [];
        foreach ($this->fibonacci(50000) as $fib) {
            $md5Tab[] = md5($fib);
        }
        $cpu = $this->getCpuUsage();
        return $this->render('default/fib.html.twig', array(
            'fib' => $md5Tab,
            'cpu' => $cpu,
        ));
    }

    function fibonacci($n,$first = 0,$second = 1)
    {
        $fib = [$first,$second];
        for($i=1;$i<$n;$i++)
        {
            $fib[] = $fib[$i]+$fib[$i-1];
        }
        return $fib;
    }

    function onRequestStart() {
        $dat = getrusage();
        define('PHP_TUSAGE', microtime(true));
        define('PHP_RUSAGE', $dat["ru_utime.tv_sec"]*1e6+$dat["ru_utime.tv_usec"]);
    }

    function getCpuUsage() {
        $dat = getrusage();
        $dat["ru_utime.tv_usec"] = ($dat["ru_utime.tv_sec"]*1e6 + $dat["ru_utime.tv_usec"]) - PHP_RUSAGE;
        $time = (microtime(true) - PHP_TUSAGE) * 1000000;

        // cpu per request
        if($time > 0) {
            $cpu = sprintf("%01.2f", ($dat["ru_utime.tv_usec"] / $time) * 100);
        } else {
            $cpu = '0.00';
        }

        return $cpu;
    }


}
